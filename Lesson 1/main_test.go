package main

import (
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestSum(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	x := rand.Int()
	y := rand.Int()

	s := sum(x, y)

	assert.Equal(t, x+y, s)
}
