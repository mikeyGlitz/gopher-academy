# Lesson 1

This lesson will get you acquainted with the basics of go. In this lesson, you will
implement a basic go program and then create a function to compute the sum of two numbers.

This lesson comes with a test which can be executed using the `go test` command from
the terminal.
