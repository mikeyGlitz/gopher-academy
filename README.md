# gopher-academy

This repository hosts a series which will take you through beginner lessons for Go lang.
The lessons contained within this repository are intended to take you from beginner to intermediate.
You will learn basic Go lang concepts and eventually transition into data structures.

## Getting started

This repository assumes you already have foundational knowledge of Linux/UNIX, how to use
version control, and how to install software onto your computer. For an easy drop-in experience
please make sure you have [Visual Studio Code](https://code.visualstudio.com) and
[Docker](https://docker.com) installed on your computer in order to use the
[Visual Studio Code Development Containers extension](https://code.visualstudio.com/docs/remote/containers).

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Roadmap

## License
This project is licensed under the GPLv3 license
